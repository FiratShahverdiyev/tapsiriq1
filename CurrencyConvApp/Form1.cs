﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CurrencyConvApp
{
    public partial class Form1 : Form
    {
        private Currency currency;
        ComponentResourceManager resources ;

        public Form1() 
        {
            InitializeComponent();
            currency = new Currency();
            resources = new ComponentResourceManager(typeof(Form1));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            exceptionButton.Text = "Converting...";
            Exchange exchange = new Exchange();
            float result = exchange.GetExchangeRate(currency.From,currency.To,currency.Amount);
            resultText.Text = result.ToString();

            exceptionButton.Text = "Converted Success";
           
            try
            {
                if (currency.Amount==0 || !fromCombo.Items.Contains(currency.From) || !toCombo.Items.Contains(currency.To) )
                {
                    throw new ArgumentException("pls valid enter number and currency");
                }
            }
            catch {
                exceptionButton.Text = "Invalid number or currency entered ! Pls try again";
                currency.Amount = 0;
            }
            
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void amountText_TextChanged(object sender, EventArgs e)
        {
            float amount = -1;
            exceptionAmountLabel.Text = "";
            exchangeButton.Text = "";
            try
            {
                amount = float.Parse(amountText.Text);
            }
            catch
            {
                if (amountText.Text != "")
                {
                    exceptionAmountLabel.Text = "Pls enter valid number!";
                    exceptionButton.Text = "";
                    currency.Amount = 0;
                    return;
                }
            }
            
            currency.Amount = amount;
           
        }

        private void fromCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            currency.From = fromCombo.Text;
            try
            {
                pictureBox1.Image = (Image)(resources.GetObject(currency.From.ToLower().Trim()+".Image"));
                exceptionButton.Text = "";
            }
            catch
            {
                exceptionButton.Text = "Invalid Currency selected ! Pls try again";
            }
        }

        private void toCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            currency.To= toCombo.Text;
            try
            {
                pictureBox2.Image = (Image)(resources.GetObject(currency.To.ToLower().Trim() + ".Image"));
                exceptionButton.Text = "";
            }
            catch
            {
                exceptionButton.Text = "Invalid Currency selected ! Pls try again";
            }

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
