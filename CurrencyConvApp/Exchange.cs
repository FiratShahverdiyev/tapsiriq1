﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConvApp
{
    class Exchange
    {
     
         public static float GetCurrencyRateInEuro(string currency)
        {
            if (currency.ToLower() == "")
                throw new ArgumentException("Invalid Argument! currency parameter cannot be empty!");
           
  
            try
            {
                string url = string.Concat("http://www.ecb.int/rss/fxref-", currency.ToLower().Trim() + ".html");

                System.Xml.XmlDocument xmlDocument = new System.Xml.XmlDocument();
                xmlDocument.Load(url);

                System.Xml.XmlNamespaceManager xmlNamespace = new System.Xml.XmlNamespaceManager(xmlDocument.NameTable);
                xmlNamespace.AddNamespace("rdf", "http://purl.org/rss/1.0/");
                xmlNamespace.AddNamespace("cb", "http://www.cbwiki.net/wiki/index.php/Specification_1.1");

                System.Xml.XmlNodeList nodeList = xmlDocument.SelectNodes("//rdf:item", xmlNamespace);

                foreach (System.Xml.XmlNode node in nodeList)
                {
                    CultureInfo cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                    cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";

                    try
                    {
                        float exchangeRate = float.Parse(
                            node.SelectSingleNode("//cb:statistics//cb:exchangeRate//cb:value", xmlNamespace).InnerText,
                            NumberStyles.Any,
                            cultureInfo);

                        return exchangeRate;
                    }
                    catch { }
                }

                return 0;
            }
            catch
            {
                return 0;
            }
        }
        public  float GetExchangeRate(string from, string to, float amount )
        {
            if (from == null || to == null)
                return 0;

            if (from.ToLower() == "eur" && to.ToLower() == "eur")
                return amount;

           
                float toRate = GetCurrencyRateInEuro(to);
                float fromRate = GetCurrencyRateInEuro(from);

                if (from.ToLower() == "eur")
                {
                    return (amount * toRate);
                }
                else if (to.ToLower() == "eur")
                {
                    return (amount / fromRate);
                }
                else
                {
                    return (amount * toRate) / fromRate;
                }
          
        }

    }
}
