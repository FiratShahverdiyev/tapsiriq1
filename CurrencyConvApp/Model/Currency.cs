﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConvApp
{
    class Currency
    {
        public string From { get; set; }
        public string To { get; set; }
        public float Amount { get; set; }
    }
}
