﻿namespace CurrencyConvApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.exchangeButton = new System.Windows.Forms.Button();
            this.amountText = new System.Windows.Forms.RichTextBox();
            this.amountLabel = new System.Windows.Forms.Label();
            this.fromCombo = new System.Windows.Forms.ComboBox();
            this.toCombo = new System.Windows.Forms.ComboBox();
            this.fromLabel = new System.Windows.Forms.Label();
            this.toLabel = new System.Windows.Forms.Label();
            this.resultText = new System.Windows.Forms.RichTextBox();
            this.resultLabel = new System.Windows.Forms.Label();
            this.exceptionButton = new System.Windows.Forms.Label();
            this.exceptionAmountLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // exchangeButton
            // 
            this.exchangeButton.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.exchangeButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.exchangeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exchangeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exchangeButton.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.exchangeButton.Image = ((System.Drawing.Image)(resources.GetObject("exchangeButton.Image")));
            this.exchangeButton.Location = new System.Drawing.Point(338, 97);
            this.exchangeButton.Name = "exchangeButton";
            this.exchangeButton.Size = new System.Drawing.Size(70, 70);
            this.exchangeButton.TabIndex = 0;
            this.exchangeButton.UseVisualStyleBackColor = false;
            this.exchangeButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // amountText
            // 
            this.amountText.Location = new System.Drawing.Point(108, 148);
            this.amountText.Name = "amountText";
            this.amountText.Size = new System.Drawing.Size(144, 30);
            this.amountText.TabIndex = 1;
            this.amountText.Text = "";
            this.amountText.TextChanged += new System.EventHandler(this.amountText_TextChanged);
            // 
            // amountLabel
            // 
            this.amountLabel.AutoSize = true;
            this.amountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountLabel.Location = new System.Drawing.Point(43, 151);
            this.amountLabel.Name = "amountLabel";
            this.amountLabel.Size = new System.Drawing.Size(59, 16);
            this.amountLabel.TabIndex = 2;
            this.amountLabel.Text = "Amount";
            // 
            // fromCombo
            // 
            this.fromCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fromCombo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromCombo.FormattingEnabled = true;
            this.fromCombo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.fromCombo.Items.AddRange(new object[] {
            "EUR",
            "USD",
            "JPY",
            "BGN",
            "CZK",
            "DKK",
            "GBP",
            "HUF",
            "LVL",
            "PLN",
            "RON",
            "SEK ",
            "CHF ",
            "NOK ",
            "HRK ",
            "RUB ",
            "TRY ",
            "AUD ",
            "BRL ",
            "CAD ",
            "CNY ",
            "HKD ",
            "IDR ",
            "ILS ",
            "INR ",
            "KRW ",
            "MXN ",
            "MYR ",
            "NZD ",
            "PHP ",
            "SGD ",
            "ZAR"});
            this.fromCombo.Location = new System.Drawing.Point(131, 92);
            this.fromCombo.Name = "fromCombo";
            this.fromCombo.Size = new System.Drawing.Size(121, 24);
            this.fromCombo.TabIndex = 3;
            this.fromCombo.SelectedIndexChanged += new System.EventHandler(this.fromCombo_SelectedIndexChanged);
            // 
            // toCombo
            // 
            this.toCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toCombo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toCombo.FormattingEnabled = true;
            this.toCombo.Items.AddRange(new object[] {
            "EUR",
            "USD",
            "JPY",
            "BGN",
            "CZK",
            "DKK",
            "GBP",
            "HUF",
            "LVL",
            "PLN",
            "RON",
            "SEK ",
            "CHF ",
            "NOK ",
            "HRK ",
            "RUB ",
            "TRY ",
            "AUD ",
            "BRL ",
            "CAD ",
            "CNY ",
            "HKD ",
            "IDR ",
            "ILS ",
            "INR ",
            "KRW ",
            "MXN ",
            "MYR ",
            "NZD ",
            "PHP ",
            "SGD ",
            "ZAR"});
            this.toCombo.Location = new System.Drawing.Point(481, 89);
            this.toCombo.Name = "toCombo";
            this.toCombo.Size = new System.Drawing.Size(121, 24);
            this.toCombo.TabIndex = 4;
            this.toCombo.SelectedIndexChanged += new System.EventHandler(this.toCombo_SelectedIndexChanged);
            // 
            // fromLabel
            // 
            this.fromLabel.AutoSize = true;
            this.fromLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromLabel.Location = new System.Drawing.Point(75, 92);
            this.fromLabel.Name = "fromLabel";
            this.fromLabel.Size = new System.Drawing.Size(43, 16);
            this.fromLabel.TabIndex = 5;
            this.fromLabel.Text = "From";
            // 
            // toLabel
            // 
            this.toLabel.AutoSize = true;
            this.toLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toLabel.Location = new System.Drawing.Point(624, 92);
            this.toLabel.Name = "toLabel";
            this.toLabel.Size = new System.Drawing.Size(27, 16);
            this.toLabel.TabIndex = 6;
            this.toLabel.Text = "To";
            // 
            // resultText
            // 
            this.resultText.Location = new System.Drawing.Point(481, 148);
            this.resultText.Name = "resultText";
            this.resultText.ReadOnly = true;
            this.resultText.Size = new System.Drawing.Size(153, 30);
            this.resultText.TabIndex = 7;
            this.resultText.Text = "";
            this.resultText.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLabel.Location = new System.Drawing.Point(656, 151);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(52, 16);
            this.resultLabel.TabIndex = 8;
            this.resultLabel.Text = "Result";
            // 
            // exceptionButton
            // 
            this.exceptionButton.AutoSize = true;
            this.exceptionButton.Location = new System.Drawing.Point(292, 223);
            this.exceptionButton.Name = "exceptionButton";
            this.exceptionButton.Size = new System.Drawing.Size(0, 13);
            this.exceptionButton.TabIndex = 9;
            // 
            // exceptionAmountLabel
            // 
            this.exceptionAmountLabel.AutoSize = true;
            this.exceptionAmountLabel.Location = new System.Drawing.Point(75, 205);
            this.exceptionAmountLabel.Name = "exceptionAmountLabel";
            this.exceptionAmountLabel.Size = new System.Drawing.Size(0, 13);
            this.exceptionAmountLabel.TabIndex = 10;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(169, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(39, 38);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(521, 45);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 38);
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.ClientSize = new System.Drawing.Size(747, 406);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.exceptionAmountLabel);
            this.Controls.Add(this.exceptionButton);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.resultText);
            this.Controls.Add(this.toLabel);
            this.Controls.Add(this.fromLabel);
            this.Controls.Add(this.toCombo);
            this.Controls.Add(this.fromCombo);
            this.Controls.Add(this.amountLabel);
            this.Controls.Add(this.amountText);
            this.Controls.Add(this.exchangeButton);
            this.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Currency Converter";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox amountText;
        private System.Windows.Forms.Label amountLabel;
        private System.Windows.Forms.ComboBox fromCombo;
        private System.Windows.Forms.ComboBox toCombo;
        private System.Windows.Forms.Label fromLabel;
        private System.Windows.Forms.Label toLabel;
        private System.Windows.Forms.RichTextBox resultText;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Label exceptionButton;
        private System.Windows.Forms.Label exceptionAmountLabel;
        private System.Windows.Forms.Button exchangeButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

